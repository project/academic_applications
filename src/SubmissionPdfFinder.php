<?php

namespace Drupal\academic_applications;

use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\academic_applications\Validator\PdfValidator;

/**
 * Class SubmissionPdfFinder finds PDFs in form submissions.
 */
class SubmissionPdfFinder {

  /**
   * Gets the file IDs of PDFs in a form submission.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webFormSubmission
   *   A Webform submission.
   *
   * @return array
   *   File IDs.
   */
  public function getFileIds(WebformSubmissionInterface $webFormSubmission): array {
    $file_ids = [];
    $pdf_elements = $this->getManagedFileElements($webFormSubmission->getWebform());
    $submission_data = $webFormSubmission->getData();
    foreach ($pdf_elements as $element) {
      if (array_key_exists($element['#webform_key'], $submission_data)) {
        $file_ids[] = $submission_data[$element['#webform_key']];
      }
    }
    return $file_ids;
  }

  /**
   * Gets form elements that store PDFs in a form submission.
   *
   * @param \Drupal\webform\WebformInterface $webForm
   *   A Webform submission.
   *
   * @return array
   *   Form elements that store PDFs.
   */
  protected function getManagedFileElements(WebformInterface $webForm): array {
    $pdf_elements = [];
    foreach ($webForm->getElementsInitializedAndFlattened() as $element) {
      if (PdfValidator::elementStoresPdf($element)) {
        $pdf_elements[] = $element;
      }
    }
    return $pdf_elements;
  }

}
