<?php

namespace Drupal\academic_applications;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\webform\WebformSubmissionListBuilder as DefaultWebformSubmissionListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides the WebformSubmissionListBuilder.
 */
class WebformSubmissionListBuilder extends DefaultWebformSubmissionListBuilder {

  /**
   * The submission bundler.
   *
   * @var \Drupal\academic_applications\SubmissionBundler
   */
  protected $submissionBundler;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->addReferencesColumn();
    $instance->submissionBundler = $container->get('academic_applications.submission_bundler');
    return $instance;
  }

  /**
   * Adds a 'references' column to the results.
   */
  protected function addReferencesColumn(): void {
    $column = [
      'title' => 'References',
      'name' => 'academic_applications_uploads',
      'format' => 'raw',
      'sort' => FALSE,
    ];
    $this->insertColumnBefore(
      'operations',
      'academic_applications_uploads',
      $column
    );
  }

  /**
   * Insert a new column before a given column.
   *
   * @param string $key
   *   The column key to insert a column before.
   * @param string $newKey
   *   The column key of the new column.
   * @param array $column
   *   The new column.
   */
  protected function insertColumnBefore(string $key, string $newKey, array $column): void {
    if (array_key_exists($key, $this->columns)) {
      $new = [];
      foreach ($this->columns as $k => $value) {
        if ($k === $key) {
          $new[$newKey] = $column;
        }
        $new[$k] = $value;
      }
      $this->columns = $new;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildRowColumn(array $column, EntityInterface $entity) {
    if ($column['name'] === 'academic_applications_uploads') {
      $links = [];
      $submissions = $this->submissionBundler->uploadFormSubmissions($entity);
      $count = 1;
      foreach ($submissions as $submission) {
        $links[] = !empty($submission->getData()['name']) ? $submission->toLink($submission->getData()['name'])->toString() : $submission->toLink($count)->toString();
        $count++;
      }
      $row = [
        'data' => [
          '#type' => 'markup',
          '#markup' => implode(', ', $links),
        ],
      ];
    }
    else {
      $row = parent::buildRowColumn($column, $entity);
    }
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $base_route_name = $this->requestHandler->getBaseRouteName($entity, $this->sourceEntity);
    $route_parameters = $this->requestHandler->getRouteParameters($entity, $this->sourceEntity);
    if ($entity->access('view')) {
      $operations['bundle'] = [
        'title' => $this->t('Bundle'),
        'weight' => -10,
        'url' => Url::fromRoute("$base_route_name.webform_submission.bundle", $route_parameters),
      ];
    }
    return $operations;
  }

}
